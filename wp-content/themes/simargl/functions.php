<?php

require 'myadmin.php';

/************* ADD EXCERPT TO PAGES *****************/

add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

/************* ADD CATEGORIES TO PAGES *****************/

function myplugin_settings() {  
// Add tag metabox to page
register_taxonomy_for_object_type('post_tag', 'page'); 
// Add category metabox to page
register_taxonomy_for_object_type('category', 'page');  
}
 // Add to the admin_init hook of your theme functions.php file 
add_action( 'admin_init', 'myplugin_settings' );


/*************  THIS GIVES US SOME OPTIONS FOR STYLING THE ADMIN AREA*****************/
function custom_admin_css() {
   echo '<style type="text/css">
           .admin-page-framework-container .admin-page-framework-section-title h3{
                border-top: 2px solid #C3C3C3 !important;
                padding-top: 20px !important;
            }
            .admin-page-framework-container .admin-page-framework-field-image{
                cursor: move;
            }
         </style>';
}

add_action('admin_head', 'custom_admin_css');