<?php
/************* ADMIN PAGE *****************/

if ( !class_exists( 'AdminPageFramework' ) )
    include_once( dirname( __FILE__ ) . '/class/admin-page-framework.min.php' );
 

class APF extends AdminPageFramework {
 
    function setUp() {
       
        $this->setRootMenuPage( 'Simragl Theme Settings',
        						null,
        						'22' 
        );  
       
        $this->addSubMenuItems(
            array(
                'title' => 'Homepage Settings',
                'page_slug' => 'homepage_settings',
            )          
        );

        $this->addSettingSections(	
        	'homepage_settings',
			array(
				'section_id'		=>	'slider_settings',	// avoid hyphen(dash), dots, and white spaces
				// 'tab_slug'		=>	'textfields',
				'title'			=>	__( 'Slider Settings', 'simargl' ),
			),	        	
			array(
				'section_id'		=>	'mediakit_settings',	// avoid hyphen(dash), dots, and white spaces
				// 'tab_slug'		=>	'textfields',
				'title'			=>	__( 'Media Kit Settings', 'simargl' ),
			),	        	
			array(
				'section_id'		=>	'slider2_settings',	// avoid hyphen(dash), dots, and white spaces
				// 'tab_slug'		=>	'textfields',
				'title'			=>	__( 'Our Partners Slider Settings', 'simargl' ),
			),	
			array(
				'section_id'		=>	'contact_settings',	// avoid hyphen(dash), dots, and white spaces
				// 'tab_slug'		=>	'textfields',
				'title'			=>	__( 'Contact Us Settings', 'simargl' ),
			)
		);     

		$this->addSettingFields( 
			'slider_settings',
			array( // Media File with Attributes
				'field_id'	=>	'slider_media',
				'title'	=>	__( 'Slider image and text', 'simargl' ),
				'type'	=>	'image',
				'repeatable'	=>	true,
				'sortable'	=>	true,				
				'attributes_to_store'	=>	array( 'id', 'title', 'caption' ),
				'attributes'	=>	array(
					'style'	=>	'max-width:100px;max-height:100px',
				),
				'description'	=>	__( 'To edit the slider texts press "Select Image", then select the image and edit "Title" as the big text and "Caption" as the small text. The slider images must have a width of 1600px and a height of 900px. Press "+" to add more sliders. Hold mouse on boxes to switch the slides order.', 'simargl' ),
			),		
            array( 
                'field_id' => 'submit_button_field',
                'section_id' => 'slider_settings',
                'type' => 'submit',
                'title' => __( 'Save Slider Changes' , 'simargl' ),
            )						
		);

		$this->addSettingFields( 
			'slider2_settings',
			array( // Media File with Attributes
				'field_id'	=>	'slider2_media',
				'title'	=>	__( 'Slider image', 'simargl' ),
				'type'	=>	'image',
				'repeatable'	=>	true,
				'sortable'	=>	true,				
				'attributes'	=>	array(
					'style'	=>	'max-width:100px;max-height:100px',
				),
				'description'	=>	__( 'The slider images must have a width of 220px and a height of 220px. Press "+" to add more sliders. Hold mouse on boxes to switch the slides order.', 'simargl' ),
			),		
            array( 
                'field_id' => 'submit_button2_field',
                'section_id' => 'slider2_settings',
                'type' => 'submit',
                'title' => __( 'Save Slider Changes' , 'simargl' ),
            )						
		);		

		$this->addSettingFields( 
			'mediakit_settings',  
			array(	// Text Area
				'field_id'	=>	'mediakit_textarea',
				'title'	=>	__( 'Media Kit text', 'simargl' ),
				'type'	=>	'textarea',
				'attributes'	=>	array(
					'rows'	=>	2,
					'cols'	=>	70,
				),
			), 	
			array( // Media File
				'field_id'	=>	'mediakit_media',
				'title'	=>	__( 'PDF File', 'simargl' ),
				'type'	=>	'media',
				'allow_external_source'	=>	false,
			), 
            array( 
                'field_id' => 'submit_button_field3',
                'section_id' => 'mediakit_settings',
                'type' => 'submit',
                'title' => __( 'Save Media Kit Changes' , 'simargl' ),
            )		 	
		);

        $this->addSettingFields(    
        	'contact_settings',
			array(	// Text Area
				'field_id'	=>	'contact_textarea',
				'title'	=>	__( 'Contact Us text', 'simargl' ),
				'type'	=>	'textarea',
				'attributes'	=>	array(
					'rows'	=>	3,
					'cols'	=>	70,
				),
			),        	
            array(  // Multiple text fields
				'field_id'	=>	'contact_form',
				'title'	=>	__( 'Contact Info', 'simargl' ),
				'help'	=>	__( 'Contact Us section information.', 'simargl' ),
				'type'	=>	'text',
				'label'	=>	'Address: ',
				'attributes'	=>	array(
					'size'	=>	60,				
				),
				'delimiter'	=>	'<br />',
				array(
					'label'	=>	'Phone ',
					'attributes'	=>	array(
						'size'	=>	40,
					)
				),
				array(
					'label'	=>	'Email: ',
					'attributes'	=>	array(
						'size'	=>	40,
					)
				)			
            ),         		
            array(  // Multiple text fields
				'field_id'	=>	'social_media_form',
				'title'	=>	__( 'Social Media Links', 'simargl' ),
				'help'	=>	__( 'Add the link of your social pages. Leave blank to not show icon.', 'simargl' ),
				'type'	=>	'text',
				'default'	=>	'http://www.facebook.com/myfacebookid',
				'label'	=>	'Facebook: ',
				'attributes'	=>	array(
					'size'	=>	40,				
				),
				'delimiter'	=>	'<br />',
				array(
					'default'	=>	'http://www.twitter.com/mytwitterid',
					'label'	=>	'Twitter: ',
					'attributes'	=>	array(
						'size'	=>	40,
					)
				),
				array(
					'default'	=>	'http://www.youtube.com/youtubeid',
					'label'	=>	'Youtube: ',
					'attributes'	=>	array(
						'size'	=>	40,
					)
				)			
            ), 
            array( 
                'field_id' => 'submit_button_field4',
                'section_id' => 'contact_settings',
                'type' => 'submit',
                'title' => __( 'Save Contact Changes' , 'simargl' ),
            )
        );                 
    }

   
}
 
new APF;