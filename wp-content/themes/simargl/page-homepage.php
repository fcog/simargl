<?php
/*
Template Name: Homepage
*/
?>

<?php get_header(); ?>
<?php $options = get_option( 'APF' ); ?>

<?php $slides = $options['slider_settings']["slider_media"] ?>

<?php if ( !empty($slides) ): ?>
<section id="slider">
    <div class="language">
        <?php //if(qtrans_getLanguage() == 'en'): ?>
            <a class="ru" href="?lang=ru" title="русский"></a>
        <?php //else: ?>
            <a class="en" href="?lang=en" title="English"></a>
        <?php //endif ?>
    </div>
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

<?php  $slides = $options['slider_settings']["slider_media"] ?>
	 
	  <!-- Indicators -->
	  <ol class="carousel-indicators">
<?php for ($i=0; $i < count($slides); $i++): ?>
	    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i ?>" <?php if ($i == 0): ?> class="active"><?php endif ?></li>
<?php endfor ?>
	  </ol>
	 
	  <!-- Slider Content (Wrapper for slides )-->
	  <div class="carousel-inner">
<?php $i = 0 ?>
<?php foreach ($slides as $slide): ?>
	    <div class="item <?php if ($i == 0): ?> active<?php endif ?>">
	      <img src="<?php echo $slide['url'] ?>" alt="Slide">
	      <div class="carousel-caption">
	      	<h2><?php echo $slide['title'] ?></h2>
	        <p><?php echo $slide['caption'] ?></p>
	      </div>
	    </div>
	<?php $i++ ?>
<?php endforeach ?>
	  </div>
	 
	  <!-- Controls -->
	  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
	    <span class="glyphicon glyphicon-chevron-left"></span>
	  </a>
	  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
	    <span class="glyphicon glyphicon-chevron-right"></span>
	  </a>
	</div>	
</section>
<?php endif ?>
<section id="media-kit">
	<div class="inner-container">
		<div id="media-kit-logo" class="col-xs-12 col-sm-3">
			<h2>Media Kit</h2>
		</div>
		<p class="col-xs-12 col-sm-6"><?php echo $options['mediakit_settings']["mediakit_textarea"] ?></p>
		<?php if ( !empty( $options['mediakit_settings']["mediakit_media"] ) ): ?>
		<div class="button-blue col-xs-12 col-sm-3"><a href="<?php echo $options['mediakit_settings']["mediakit_media"] ?>" title="Download PDF">Download PDF</a></div>
		<?php endif ?>
	</div>
</section>
<?php
$the_slug = 'what-we-do';
$args=array(
  'name' => $the_slug,
  'post_type' => 'page',
  'post_status' => 'publish',
  'numberposts' => 1
);
$post = get_posts($args);
if( $post ):	
?>
	<section id="what-we-do">
		<div class="inner-container">
			<h2>What <span>We Do</span></h2>
			<div id="what-we-do-images">
				<div class="what-we-do-image image1"><img src="wp-content/themes/simargl/images/button1-what.png" alt="Reputation Management"></div>
				<div class="what-we-do-image image2"><img src="wp-content/themes/simargl/images/button2-what.png" alt=""></div>
				<div class="what-we-do-image image3"><img src="wp-content/themes/simargl/images/button3-what.png" alt=""></div>
				<div class="what-we-do-image image4"><img src="wp-content/themes/simargl/images/button4-what.png" alt=""></div>
				<div class="what-we-do-image image5"><img src="wp-content/themes/simargl/images/button5-what.png" alt=""></div>
			</div>
			<p><?php echo $post[0]->post_excerpt ?></p>
		</div>
	</section>
<?php endif ?>
<?php
$category_id = get_cat_ID('services');
$loop = new WP_Query( array('post_type' => 'page', 'cat' => $category_id, 'order'=> 'ASC') );
if( count($loop) > 0 ):	
?>
	<section id="our-services">
		<h2>Our <span>Services</span></h2>
		<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<div class="service row">
				<div class="inner-container">
					<div class="service-image col-xs-12 col-sm-3"><?php the_post_thumbnail('full') ?></div>
					<div class="service-text col-xs-12 col-sm-9">
						<h3><?php the_title() ?></h3>
						<?php the_excerpt() ?>
						<div class="service-button"><a href="<?php the_permalink() ?>" title="View more">View more</a></div>
					</div>
				</div>
			</div>	
		<?php endwhile ?>		
	</section>
<?php endif ?>
<?php
$the_slug = 'where-we-work';
$args=array(
  'name' => $the_slug,
  'post_type' => 'page',
  'post_status' => 'publish',
  'numberposts' => 1
);
$post = get_posts($args);
if( $post ):	
?>
<section id="where-we-work">
	<div class="world-container">
		<img src="wp-content/themes/simargl/images/bg-map.png" alt="World Map">
		<div class="text-container">
			<h2>Where <span>We Work</span></h2>
			<p><?php echo $post[0]->post_excerpt ?></p>
			<img src="wp-content/themes/simargl/images/map_off.png" alt="Where we work" id="map" usemap="#image-map">
		</div>
	</div>
</section>
<?php endif ?>
<?php
$the_slug = 'who-we-are';
$args=array(
  'name' => $the_slug,
  'post_type' => 'page',
  'post_status' => 'publish',
  'numberposts' => 1
);
$post = get_posts($args);
if( $post ):	
?>
<section id="who-we-are">
	<img src="wp-content/themes/simargl/images/bg-who.jpg" alt="">
	<div class="text-container col-xs-8 col-sm-3">
		<h2>Who <span>We Are</span></h2>
		<p><?php echo $post[0]->post_excerpt ?></p>
	</div>
</section>
<?php endif ?>
<?php
$the_slug = 'our-partners';
$args=array(
  'name' => $the_slug,
  'post_type' => 'page',
  'post_status' => 'publish',
  'numberposts' => 1
);
$post = get_posts($args);
if( $post ):	
?>
<section id="our-partners">
	<div class="inner-container">
		<div class="text-wrapper col-xs-12 col-sm-8">
			<div class="text-container">
				<h2>Our <span>Partners</span></h2>
				<p><?php echo $post[0]->post_excerpt ?></p>
			</div>
		</div>
		<?php $slides = $options['slider2_settings']["slider2_media"] ?>
	  	<?php if ( !empty($slides) ): ?>
		 	<div class="carousel2 col-xs-6 col-sm-3">
		 		<div id="carousel2-example-generic" class="carousel slide" data-ride="carousel">
				    <div class="carousel-inner">
				<?php $i = 0 ?>
				<?php foreach ($slides as $slide): ?>
					    <div class="item <?php if ($i == 0): ?>active<?php endif ?>">
					      <img src="<?php echo $slide ?>" alt="Slide">
					    </div>
					<?php $i++ ?>
				<?php endforeach ?>
				 	</div>
				 
					<!-- Controls -->
					<a class="left carousel-control" href="#carousel2-example-generic" data-slide="prev">
					    <span class="glyphicon glyphicon-chevron-left"></span>
					</a>
					<a class="right carousel-control" href="#carousel2-example-generic" data-slide="next">
					    <span class="glyphicon glyphicon-chevron-right"></span>
					</a>	
				</div>	
			</div>
		<?php endif ?>
	</div>
</section>
<?php endif ?>
<section id="contact-us">
	<h2>Contact <span>Us</span></h2>
	<div class="inner-container">
		<div class="contact-form col-xs-12 col-sm-6">
			<?php echo do_shortcode('[contact-form-7 id="26" title="Contact form 1"]'); ?>
		</div>
		<div class="contact-text col-xs-12 col-sm-6">
			<p><?php echo $options['contact_settings']["contact_textarea"] ?></p>
			<?php $contact_info = $options['contact_settings']["contact_form"]; ?>
			<?php if (!empty($contact_info[0])): ?><address><?php echo $contact_info[0] ?></address><?php endif ?>
			<?php if (!empty($contact_info[1])): ?><div class="phone"><?php echo $contact_info[1] ?></div><?php endif ?>
			<?php if (!empty($contact_info[2])): ?><div class="email"><?php echo $contact_info[2] ?></div><?php endif ?>
	        <ul class="social-buttons">
            
            <?php $social_media_links = $options['contact_settings']["social_media_form"]; ?>
            <?php if (!empty($social_media_links[0])): ?><li class='facebook'><a href='<?php echo $social_media_links[0] ?>' target='_blank'></a></li><?php endif ?>
            <?php if (!empty($social_media_links[1])): ?><li class='twitter'><a href='<?php echo $social_media_links[1] ?>' target='_blank'></a></li><?php endif ?>
            <?php if (!empty($social_media_links[2])): ?><li class='youtube'><a href='<?php echo $social_media_links[2] ?>' target='_blank'></a></li><?php endif ?>
            </ul>
		</div>
	</div>
</section>

<script type="text/javascript">
jQuery(document).ready(function($){

	//menu anchor easing
	function scrollToAnchor(aid){
		var aTag = $("#"+aid);
		$('html,body').animate({scrollTop: aTag.offset().top - 80},'slow');
	}
	$("#menu-main-menu a").click(function() {
		var href = $(this).attr('href').replace('#', '')
		scrollToAnchor(href);
	});			

	// Map hoover
	$('.text-container img').hover(
          function(){ this.src = this.src.replace("_off","_on"); },
          function(){ this.src = this.src.replace("_on","_off");
     });
	$('#image-map').hover(function(){
		$('.text-container img').attr('src' , $('.text-container img').attr('src').replace("_off","_on") );
	});
});
</script>

<?php get_footer(); ?>