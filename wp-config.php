<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dialogi_simargl');

/** MySQL database username */
define('DB_USER', 'dialogi_simargl');

/** MySQL database password */
define('DB_PASSWORD', '/gsUhs%shT');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'S}5;@okXNxQ4TNh&qYbLlw#69?Ymh?_X/q{.4NKhTKb$s![*n?8#<-c}090o<bRQ');
define('SECURE_AUTH_KEY',  'Kkp:I`<| /y!_pY]tW8mAO?-rndnLYx(2$X(2:|)r%M?uPAf_yp3mo8j;#tT_-1%');
define('LOGGED_IN_KEY',    'PC~I&IyTOX0n:*qt)7;%%4+g[qZ+,s&g-V;TTY^o[V2na^VJ;lY8fiSTNCd;_c_E');
define('NONCE_KEY',        '~+%5Xsnw~GyW}yg|7;UN/ -Zz{)HZtb!`Z6Q@r3[r$OBd~IG=I-]?Wi^4*7.h5+K');
define('AUTH_SALT',        '{+t1r-Ns+oGH$=N/2W*Ai7A&Heikit?!)%`,v%O(eT>,0%% ,&M^osx`/=.@bWw[');
define('SECURE_AUTH_SALT', '}k|uyU<U-vCZ0eA-Dq,9{9R/CH_+J${j2:1~Q+#ANimbIXdrx,z63%GIx#*/s+V7');
define('LOGGED_IN_SALT',   'bI6OM.~+|hW7nwMSqP]AS7jR3`_,+|g?Huc_Ejth!%GBJi491C7gBfWdZ|p;1S2;');
define('NONCE_SALT',       'rQbs}Kz`Mn-qnX>!-LWkva&j|>,]#nr@C.DtSd.C7W1[-d1V}4?3Kg&2|iWn3VJF');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sgl_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
